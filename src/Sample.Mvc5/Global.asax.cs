﻿using JavaScriptViewEngine;
using System;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sample.Mvc5
{
    public class MvcApplication : HttpApplication
    {
        private readonly IRenderEngineFactory _renderEngineFactor;

        public MvcApplication()
        {
            var options = new NodeRenderEngineOptions
            {
                ProjectDirectory = HostingEnvironment.MapPath("~/Node"),
            };
            var nodeRenderOptions = new Options<NodeRenderEngineOptions>(options);
            _renderEngineFactor = new SingletonRenderEngineFactory(new NodeRenderEngineBuilder(nodeRenderOptions));
            BeginRequest += OnBeginRequest;
            EndRequest += OnEndRequest;
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ViewEngines.Engines.Add(new JsViewEngine(new Options<JsViewEngineOptions>(new JsViewEngineOptions())));
        }

        private void OnBeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Items["RenderEngine"] = _renderEngineFactor.RequestEngine();
        }

        private void OnEndRequest(object sender, EventArgs e)
        {
            _renderEngineFactor.ReturnEngine((IRenderEngine)HttpContext.Current.Items["RenderEngine"]);
        }
    }
}
