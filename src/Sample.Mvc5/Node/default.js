var ReactDOMServer = require('react-dom/server');
var React = require("react");
var e = React.createElement;

var html = ReactDOMServer.renderToString(e('div', null, 'Hello World'));
//var html = "hej";

module.exports = {
    renderView: function (callback, path, model, viewBag, routeValues) {
        callback(null, {
            //html: "<html><head></head><body><h1>renderView</h1><p><strong>Model:</strong> " + JSON.stringify(model) + "</p><p><strong>ViewBag:</strong> " + JSON.stringify(viewBag) + "</p></body>",
            html: html,
            status: 200,
            redirect: null
        });
    }
};